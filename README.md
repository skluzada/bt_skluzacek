# Model-agnostic methods for explaining local predictions of a black-box classifier

Author: Adam Skluzáček
Supervisor: Ing. Maréta Jůzlová

Local model-agnostic explanation methods aim to explain a single prediction of an arbitrary machine learning model by studying the model only through its inputs and corresponding outputs. Explaining predictions of a complex machine learning model helps practitioner to debug the model and build user's trust in the predictions. 
This thesis reviews and describes three of the state-of-the-art local model-agnostic explanation methods -- LIME, Anchors and SHAP. The described methods are evaluated in terms of faithfulness of their explanaions to the model being explained. Evaluation is performed on various classifiers trained on artificially generated datasets as well as a real-world divorce dataset. The artificial datasets are generated based on known dependencies which allows to calculate optimal explanations and compare them to the explanations produced by the explanation methods. The experiments show that SHAP is the most robust out of the considered explanation methods. LIME and Anchors fail to produce faithful explanations in specific cases, however, they both managed to produce faihful explanations in experiment with real-world dataset.

# Bachelor Thesis Contents

## Experiments

To run the experiments, conda environment file with all the required dependencies is provided.

To install the conda environment:
```
conda env create --file thesis_env.yml --name env_name
```

To run the conda environment and the Jupyter Lab:
```
conda activate env_name
jupyter lab
```

The ```artificial_datasets_experiments.ipynb``` notebook contains experiments with artificial data (includes data generating process) and the ```divorce_dataset_experiments.ipynb``` notebook contains experiment with a real-world divorce dataset (the dataset is located in the dataset folder in csv format).


## Text
Compiled pdf file with the thesis as well as all the LaTeX source files are located in the text folder

