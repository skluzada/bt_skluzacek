
\subsection{Anchors}\label{sec:Anchors}
Anchors is a local model-agnostic explanation method created by the same researchers as LIME - Ribeiro et. al. \cite{ribeiro2018anchors}. Anchors explain a given prediction of a Black-box classifier by producing a Decision Rule. The motivation behind the method is that explanations from other local explanation methods have unclear coverage. For example a local explanation produced by the LIME method can be locally faithful, however, it is unclear whether the explanation also applies to some other instance. Ribeiro et. al. argue that the unclear coverage can mislead the audience as they may think that the explanation applies to some other instance when it does not. Anchors aim to produce a Decision Rule such that features that are not included in the rule's predicates does not influence the prediction of the model being explained. Such explanation have clear coverage as it applies to all instances that satisfy the rule's conditions as illustrated in \autoref{fig:anchors}. Decision Rules are also highly interpretable for humans which makes them a goot fit for explanations \cite{su2015interpretable}.

\begin{figure}[ht!]
\centering
\begin{tikzpicture}
\begin{axis}[
    width=\textwidth,
    height=8cm,
    axis x line*=bottom,
    axis y line*=left,
    axis lines=left,
    xmin=0,
    xmax=1,
    ymin=0,
    ymax=1,
    xtick=\empty,
    ytick=\empty,
    xticklabel=\empty,
    yticklabel=\empty,
]
    \addplot[mark=none, draw=green!9, fill=green!9] plot coordinates{
    (0.002, 0.005)
    (0.002, 0.98)
    (0.987, 0.98)
    (0.987, 0.005)
    (0.002, 0.005)
    };
    \addplot[mark=none, smooth, draw=red!12, fill=red!12] plot coordinates{
    (0.08, 0.81)
    (0.2, 0.62)
    (0.03, 0.55)
    (0.07, 0.12)
    (0.31, 0.07)
    (0.30, 0.24)
    (0.40, 0.50)
    (0.53, 0.80)
    (0.60, 0.65)
    (0.63, 0.14)
    (0.72, 0.16)
    (0.77, 0.50)
    (0.95, 0.58)
    (0.98, 0.93)
    (0.88, 0.82)
    (0.62, 0.93)
    (0.44, 0.95)
    (0.20, 0.80)
    (0.12, 0.95)
    (0.07, 0.89)
    (0.06, 0.83)
    };
    \addplot[
        scatter,
        only marks,
        point meta=explicit symbolic,
        scatter/classes={
            m={mark=-, red, mark size=8pt}
        },
    ] table [meta=label] {
    x       y       label
    0.23    0.32    m
    };
    \addplot[mark=none, violet, style=dashed, thick] coordinates {
    (0.05, 0.063) (0.302, 0.063)
    (0.302, 0.592) (0.04, 0.592)
    (0.04, 0.063)};
    \node[violet!70, font=\large] at (axis cs:0.28,0.11) {{$g$}};
    \node[green!70, font=\large] at (axis cs:0.9,0.06) {{$f$}};
\end{axis}
\end{tikzpicture}
\caption[Illustration of Anchors explanation]{Illustration of Anchors explanation. The green and red background shows the decision boundary of the underlying Black-box classifier~$f$, the point in the space shows the instance being explained $x$ and the explanation rule~$g$ produced by the Anchors method is illustrated as the purple rectangle. The rule's coverage is clear as it only applies to instances inside the rectangle (rule's scope) where the rule is also highly efficient.}
\label{fig:anchors}
\end{figure}

Explanations produced by Anchors are essentially local surrogate models with Decision Rules as the family of explanatory machine learning models. However the method is not based on the local surrogate explanation model framework described in \hyperref[sec:lime_fw]{subsection 1.8.1.1}. Instead, the method utilizes techniques from reinforcement learning and graph search in order to be computationally efficient.

Formally, let $f:\mathbb{R}^n\mapsto{}\mathbb{R}$ denote the model being explained, $x\in\mathbb{R}^n$ denote the input of the prediction being explained and $f(x)$ denote the corresponding prediction (which must be a class label). Let $A$ be a Decision Rule, such that $A(x)$ returns 1 if $x$ satisfies all the rule's predicates. Moreover, let $\mathcal{D}$ denote a distribution from which the training samples for the explanation are drawn and let $\mathcal{D}(\cdot|A)$ denote a conditional distribution when the rule $A$ is satisfied. For a desired level of precision $\tau$, explanation rule $A$ must satisfy the following:
\begin{equation}\label{eq:anchor1}
A(x) = 1,\, \mathbb{E}_{\mathcal{D}(z|A)}[\mathbbm{1}_{f(x)=f(z)}]\geq\tau\text{,}
\end{equation}
where $\mathbb{E}_{\mathcal{D}(z|A)}[\mathbbm{1}_{f(x)=f(z)}]=prec(A)$. For a highly dimensional input space it is infeasible to evaluate $\mathbbm{1}_{f(x)=f(z)}$ for all $z\in\mathcal{D}(\cdot|A)$.  Therefore a probabilistic precision is evaluated instead with parameter $\delta\in(0, 1)$ specifing the desired level of statistical confidence:
\begin{equation}\label{eq:anchor2}
P(prec(A)\geq\tau)\geq1-\delta\text{.} 
\end{equation}
From all the rules that satisfy this precision condition, the explanation is selected as the rule $A$ with the highest coverage $cov(A)$:

\begin{equation}\label{eq:anchor3}
\max_{A\:s.t.\,P(prec(A)\geq\tau)\geq1-\delta} \mathbb{E}_{\mathcal{D}(z)}[A(z)]\text{,}
\end{equation}
where $\mathbb{E}_{\mathcal{D}(z)}[A(z)] = cov(A)$. Through maximizing the coverage, interpretability of the explanation is indirectly also maximized as Decision Rules with less predicates tend to have higher coverage. On the other hand, Decision Rules with more predicates tend to have lower coverage and higher precision which yields a trade-off between precision and coverage. 

Evaluating $A(z)$ for all $z\in\mathcal{D}$ is infeasible, therefore the $cov(A)$ is estimated based on a fixed number of samples as the ratio of samples that satisfy the given rule $A$. However, solving \autoref{eq:anchor3} directly remains intractable as the number of all possible rules grows exponentially with $n$ (number of features). 

In order to produce an explanation that is an approximation of the \autoref{eq:anchor3}, Beam search over the space of potential explanations is performed. Beam~search is a search algorithm that uses a heuristic to expand only $B$ best candidates in each depth \cite{foldoc2007beam}. The Beam search therefore have two main parts. In the first part, a new set of rule candidates is generated out of the set of $B$ best rule candidates from the previous step and in the second part, new $B$ best rule candidates with the highest precision are selected for the next step of the Beam search. Algorithms for both of these parts are described below.

\textbf{Generating rule candidates by expanding rules}\quad The pseudocode of generating rule candidates is given in \hyperref[alg:gencads]{Algorithm 3}. The algorithm starts with an empty set of rules $\mathcal{A}_{cand}$ and in each iteration, each rule $A\in\mathcal{A}$ is extended by one additional feature predicate for each predicate $a_i$, such that $a_i$ is satisfied by $x$ and $cov(A \wedge a_i) > cov(A^*)$, where $A^*$ denotes the best rule found so far with respect to the \autoref{eq:anchor3}. This condition prunes the search space as the rule's coverage can only decrease when adding more predicates. For the purpose of generating  feature predicates, continuous features are discretized into bins of equal height (quartiles or deciles) as predicates are generated for each value of each feature. 

\newpage

\begin{algorithm}[H]
\caption{Generating rule candidates by expanding rules} \label{alg:gencads}
\begin{algorithmic}[1]
\Require Input $x$ of the prediction being explained
\Require Set of the current candidate rules $\mathcal{A}$
\Require Coverage of the best rule found so far $cov(A^*)$
\Function{GenerateCandidates}{$x$, $\mathcal{A}$, $cov(A^*)$}
\State $\mathcal{A}_{cand} \leftarrow \emptyset$
    \ForAll{$A\in\mathcal{A}$}
        \ForAll{$a_i$ s.t. $a_i$ is satisfied by $x$ and $a_i \notin A$}
            \If{$cov(A\wedge a_i) > cov(A^*)$} \Comment{Prunning of the search space}
                \State $\mathcal{A}_{cand} \leftarrow \mathcal{A}_{cand} \cup (A\wedge a_i)$
            \EndIf
        \EndFor
    \EndFor
\State \textbf{return} $\mathcal{A}_{cand}$  
\EndFunction
\end{algorithmic}
\end{algorithm}

\textbf{Selecting the best candidate rules}\quad After generating the set of rules $\mathcal{A}$, $B$ candidate rules with the highest precision from $\mathcal{A}_{cand}$ have to be selected for the next iteration. As disscused earlier, it is infeasible to evaluate all the samples from $\mathcal{D}(\cdot|A)$ to calculate the true precision of a rule $A$ and therefore an approximation have to be calculated instead. The approximation of the rule's~$A$ precision is calculated on a limited number of samples $z\in\mathcal{D}(\cdot|A)$. Choosing a fixed number of samples may lead to inaccurate estimations in some cases and high computational complexity in other cases. The goal is to accurately approximate precisions using a minimal number of samples from~$\mathcal{D}$. To achieve this goal, the problem of selecting $B$ rules with the highest precision is reformulated as an instance of explore-$m$ multi-armed bandit problem. 

Multi-armed bandit problem is a reinforcement learning problem inspired by a gambler who can choose from a number of one-armed bandit slot machines. Each slot machine yields a reward with a different probability. The reward probabilities are unknown to the gambler, however, he is allowed to play the slot machines arbitrarily. In the explore-$m$ setting, gambler's goal is to identify a subset of the highest rewarding slot machines with a minimal number of plays~\cite{weber1992gittins}. Formally, consider a finite number of arms $K\geq2$, where each sample of arm $k$ yields a reward generated from a Bernoulli distribution with mean $p_k\in[0, 1]$. The arms can be sorted based on the corresponding means such that $p_1 \geq p_2 \geq \dots \geq p_K$. The goal is to identify $m$ arms with the highest means using a minimal number of samples. In the context of selecting rules with the highest precision, each arm~$k$ corresponds to a rule $A$, mean $p_k$ corresponds to precision $prec(A)$ and sampling arm $k$ corresponds to drawing a sample~$z\in\mathcal{D}(\cdot|A)$ and evaluating $\mathbbm{1}_{f(x)=f(z)}$~\cite{kalyanakrishnan2012pac}.

To solve this task, Ribeiro et. al. proposed to use the KL-LUCB algorithm. Informally, the KL-LUCB algorithm works by constructing confidence regions for the precision estimate of each rule. In each iteration, the current set of $B$ best rules is selected and only the two critical rules are sampled - the worst rule from the set of the best rules and the best rule outside of the set of the best rules. The iterations stop when the critical rules' confidence regions are far enough apart and the current set of $B$ best rules is returned.

The pseudocode of KL-LUCB algorithm is given in \hyperref[alg:kl-lucb]{Algorithm 4}. In the first step, the algorithm samples~$z\in\mathcal{D}(\cdot|A)$ for each rule $A$ to initialize the precision estimate $\widehat{prec}(A)$ as well as confidence region's bounds $prec_{lb}(A)$ and $prec_{ub}(A)$ computed based on Kullback-Leibler divergence:

\begin{equation}\label{eq:kl_div1}
\begin{split}
prec_{lb}(A) = min\{q\in[0,\widehat{prec}(A)]:N_A(t)KL(\widehat{prec}(A),q) \leq \beta(t, \delta)\}\text{,} \\
prec_{ub}(A) = max\{q\in[\widehat{prec}(A),1]:N_A(t)KL(\widehat{prec}(A),q) \leq \beta(t, \delta)\}\text{,}
\end{split}
\end{equation}

where $t$ denotes the time (step of the algorithm), $N_A(t)$ denotes the number of samples $z\in\mathcal{D}(\cdot|A)$ drawn up to time t (number of updates of $\widehat{prec}(A)$), $\delta$ denotes the desired level of statistical confidence on the precision estimate, $KL$~denotes the Kullback-Leibler divergence between two Bernoulli distributions, given as $KL(p_x, p_y) = p_x \text{log}(\frac{p_x}{p_y}) + (1-p_x) \text{log}(\frac{1-p_x}{1-p_y})$ and $\beta$ denotes the exploration rate which is by default defined as $\beta(t, \delta)=log(\frac{k t^\alpha K}{\delta})~+~log\,log(\frac{k t^\alpha K}{\delta})$, where $\alpha > 1$ and $k > 1 + \frac{1}{\alpha-1}$ are parameters~\cite{kaufmann2013information}. 

Then in each iteration, the algorithm selects the set $\mathcal{A}_{best}$ of $B$ rules with the highest precision estimates and two critical rules $A_u$ and $A_l$ such that:
\begin{equation}\label{eq:kl_div1}
A_u = \argmax_{A \notin \mathcal{A}_{best}} prec_{ub}(A)\text{, } A_l = \argmin_{A \in \mathcal{A}_{best}} prec_{lb}(A)
\end{equation}
In other words, rule $A_u$ is the rule with the highest precision upper bound outside of $\mathcal{A}_{best}$ and rule $A_l$ is the rule with he lowest precision lower bound from $\mathcal{A}_{best}$. In each iteration, only the two critical rules are sampled and updated and the iterations stops after the difference between the precision upper bound of $A_u$ and the precision lower bound of $A_l$ is lower than a selected tolerance $\epsilon \in [0,1]$. The KL-LUCB algorithm returns a set $\mathcal{A}_{best}$ of $B$ rules that satisfy the following (proved in \cite{kaufmann2013information}):

\begin{equation}\label{eq:kl_div2}
P(\min_{A \in \mathcal{A}_{best}} prec(A) \geq \min_{A' \in \mathcal{A'}_{best}} prec(A') - \epsilon) \geq 1 - \delta \text{,} 
\end{equation}
where $\mathcal{A'}_{best}$ is the true set of $B$ rules with the highest precision.

\newpage

\begin{algorithm}[H]
\caption{Selecting B best rule cadidates via KL-LUCB algorithm}\label{alg:kl-lucb}
\begin{algorithmic}[1]
\Require Classifier $f$, Input $x$ of the prediction being explained
\Require Distribution $\mathcal{D}$ of the training data for explanation 
\Require Set of rules $\mathcal{A}$, Number of rules to be chosen $B$
\Require Desired level of statistical confidence $\delta$
\Require Tolerance $\epsilon$, Exploration rate $\beta$
\Function{B-BestCandidates}{$f, x, \mathcal{A}, \mathcal{D}, B, \delta, \epsilon, \beta$}
\ForAll{$A\in\mathcal{A}$} \Comment{Initialization}
    \State \textbf{sample} $z\in\mathcal{D}(\cdot|A)$
    \State \textbf{initialize} $\widehat{prec}(A), prec_{lb}(A), prec_{ub}(A)$ 
    \State \textbf{initialize} $\mathcal{A}_{best}$, $A_u$, $A_l$
\EndFor
\While{$prec_{ub}(A_u) - prec_{lb}(A_l) > \epsilon$}
    \State \textbf{sample} $z_u\in\mathcal{D}(\cdot|A_u)$
    \State \textbf{update} $\widehat{prec}(A_u), prec_{lb}(A_u), prec_{ub}(A_u)$
    \State \textbf{sample} $z_l\in\mathcal{D}(\cdot|A_l)$
    \State \textbf{update} $\widehat{prec}(A_l), prec_{lb}(A_l), prec_{ub}(A_l)$
    \State \textbf{update} $\mathcal{A}_{best}$, $A_u$, $A_l$    
\EndWhile
\State \textbf{return} $\mathcal{A}_{best}$ 
\EndFunction
\end{algorithmic}
\end{algorithm}



The final pseudocode of the Beam search for generating the explanation rule is given in \hyperref[alg:anchors]{Algorithm 5}. The Beam search iteratively search the space of potential candidate rules using the \textsc{GenerateCandidates} method for generating a new set of rule cadidates and \textsc{B-BestCandidates} method for selecting the $B$ rules with the highest precision. In the end of each iteration, the best rule $A^*$ is potentially updated as the rule with the precision lower bound higher than the desired level of rule precision $\tau$ and the highest coverage found so far. The search ends when there is no rule candidate that could potentially surprass the best rule found so far.

\newpage

\begin{algorithm}[H]
\caption{Anchors - Beam Search} \label{alg:anchors}
\begin{algorithmic}[1]
\Require Classifier $f$, Input $x$ of the prediction being explained
\Require Distribution $\mathcal{D}$ of the training data for explanation 
\Require Beam width $B$
\Require Desired levels of precision $\tau$ and statistical confidence $\delta$
\Require Tolerance $\epsilon$, Exploration rate $\beta$
\State $\mathcal{A}_{0} \leftarrow \emptyset$ \Comment{Initialize the set of candidate rules}
\State $A^* \leftarrow$ \textbf{null} \Comment{Initialize the best rule found so far}
\Loop
    \State ${\mathcal{A}_{t}}_{cand} \leftarrow$ \textsc{GenerateCandidates}$(x, \mathcal{A}_{t-1}, cov(A^*))$
    \State $\mathcal{A}_{t} \leftarrow$ \textsc{B-BestCandidates}$(f, x, {\mathcal{A}_{t}}_{cand}, \mathcal{D}, B, \delta, \epsilon, \beta)$
    \If{$\mathcal{A}_{t} = \emptyset$} \Comment{No candidate rules left}
        \State \textbf{break loop}
    \EndIf
    \ForAll{$A\in \mathcal{A}_{t}$ s.t. $prec_{lb}(A) > \tau$} \Comment{Satisfy \autoref{eq:anchor2}}
        \If{$cov(A) > cov(A^*)$} \Comment{Highest coverage found so far}
            \State $A^* \leftarrow A$
        \EndIf
    \EndFor
\EndLoop
\State \textbf{return} $A^*$ 
\end{algorithmic}
\end{algorithm}

\vskip 0.2cm
