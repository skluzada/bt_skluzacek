\section{Faithfulness evaluation on artificial datasets}\label{sec:eval_ad}

In this section, the local model-agnostic explanation methods described in \hyperref[sec:local]{Section 1.7} are experimentally evaluated on artificial datasets in terms of their faithfulness . Faithfulness refers to the accuracy of an explanation with respect to the model being explained. Artificial datasets are generated based on known predefined dependencies, which allows for the optimal explanations to be calculated. Explanations of machine learning models that are highly accurate on a given artificial dataset can then be compared to the optimal explanation in order to evaluate explanation's faithfulness. 

\subsection{Artificial datasets}

The artificial datasets described below are inspired by Robnik-Šikonja~et.~al. who use artificial datasets to evaluate their local model-agnostic explanation method in~\cite{RobnikSikonja2008ExplainingCF}. Each artificial dataset is associated with a suitable family of machine learning models.

\textbf{condInd}\quad The condInd dataset consists of eight binary features and binary class with 50\% probability of 1. Four of the features are important as they have equal value to the class in 90, 80, 70 and 60\% of the cases, respectively. The other four features are unimportant as they are unrelated to the class (have equal value to the class in 50\% cases). Since the features are conditionally independent, Naive Bayes classifier is suitable for this dataset.

\textbf{xor}\quad The xor dataset consists of six binary features and binary class. Three of the features are important and -- class is equal to 1 when an odd number of important features are equal to 1, and 0 otherwise. The other three features are unimportant as they are unrelated to the class. Decision Tree is suitable for this dataset as it can capture the necessary conditions.

\textbf{cross}\quad The cross dataset consists of four continuous features and binary class. Two of the features are important and they are generated into a cross sign as shown in \autoref{fig:cross}. The class value is 1 if $(I_1-0.5)(I_2-0.5)>0$, where $I_1$ and $I_2$ denote the two important features. The other two features are unimportant as they are unrelated to the class. Random Forest is the chosen model with the best performance on this dataset. 

\textbf{groups}\quad The groups dataset consists of four continuous features and binary class. Two of the features are important. The datapoints in 2-dimensional space of these important features form 16 clusters, where adjacent clusters have different class values as shown in~\autoref{fig:groups}. The other two features are unimportant as they are unrelated to the class. Since each group is clustered together, k-Nearest Neighbors algorithm is suitable for this dataset.

\newpage

\begin{figure}[h!] 
\centering
\begin{tikzpicture}
\begin{axis}[height=8.5cm, width=8.5cm]
\addplot[only marks, color=red!50, mark=-] table {cross0.txt};
\addplot[only marks, color=green!50, mark=+] table {cross1.txt};
\end{axis}
\end{tikzpicture}
\caption[Visualization of the cross dataset]{Visualization of the two important features in the cross dataset. Red minuses represent data points of class 0 and green pluses represent data points of class 1. }
\label{fig:cross}
\end{figure}

\begin{figure}[ht!] 
\centering
\begin{tikzpicture}
\begin{axis}[height=8.5cm, width=8.5cm]
\addplot[only marks, color=red!50, mark=-] table {groups0.txt};
\addplot[only marks, color=green!50, mark=+] table {groups1.txt};
\end{axis}
\end{tikzpicture}
\caption[Visualization of the groups dataset]{Visualization of the two important features in the groups dataset. Red minuses represent data points of class 0 and green pluses represent data points of class 1. }
\label{fig:groups}
\end{figure}

\newpage

\subsection{Metrics}\label{ad_metrics}

Metrics that are used for the evaluation of faithfulness on artificial datasets are described below. Note that since each of the evaluated explanation methods produce different type of explanation not all of the metrics are applicable on every evaluated explanation method.

\textbf{Recall on important features and False discovery rate on unimportant features}\quad Each artificial dataset is designed such that it contains equal number of important and unimportant features. This allows the explanation methods to be evaluated based on the ratio of important and unimportant features included in their explanations. For this evaluation, LIME's parameters are set such that its explanations include number of features equal to the number of important features in a given artificial dataset. Anchors and SHAP methods does not allow to set the number of features in their explanations. Anchors produces sparse explanations in order to maximize coverage of the decision rules. Features used in the decision rule explanations are considered important by the Anchors method. SHAP, however, always includes all of the features in its explanations, therefore a Shapley value threshold above which a feature is considered to be included in the explanation was experimentally set to 0.05. Higher threshold value leads to higher recall on important features and higher false discovery rate on unimportant features and vice versa.

\textbf{Mean absolute error}\quad The true feature importance scores are known for all the artificial datasets. The correct feature importance scores in absolute values are the following: 0.4, 0.3, 0.2 and 0.1 for important features in the condInt dataset, 1/3 for each important feature in the xor dataset and 1/2 for each important feature in both cross and groups datasets. The unimportant features should be assigned zero feature importance scores in all the artificial datasets.  Both LIME explanations' feature contributions and SHAP explanations' Shapley values can therefore be evaluated based on mean absolute error:

\begin{equation}\label{eq:mae}
\frac{\varSigma_{i=1}^{n}\textbar c_i - w_i \textbar}{n}\text{,}
\end{equation}

where $c_i$ denotes the correct feature importance of feature $i$ and $w_i$ denotes the explanation's feature importance of feature $i$. Note that the correct feature importance scores have their sums normalized to 1, therefore the explanations' feature importance scores sums have to be also normalized to~1 (excluding the intercept). For this evaluation, LIME's parameters are set such that its explanations include all the features. SHAP explanations' always include all the features and Anchors explanations' can not be evaluated by this metric. 

\textbf{Accuracy and Coverage}\quad LIME and Anchors methods produce local surrogate models as explanations. Local surrogate models can be evaluated based on accuracy of their approximation of the model being explained. Accuracy of a local surrogate model $g$ and a model being explained $f$ is given by:
\begin{equation}\label{eq:acc}
\frac{1}{\textbar\Lambda_{test}\textbar} \sum_{x \in \Lambda_{test}} \mathbbm{1}_{g(x) = f(x)} \text{,}
\end{equation}

where $\Lambda_{test} \subseteq \Pi_{test}$ denotes the set of testing data for the local surrogate model, $\Pi_{test}$ denotes the set of all the testing  data and $\textbar \Lambda_{test} \textbar$ denotes the number of elements in the set. Local surrogate model aims to approximate the model being explained locally, which means that the explanation does not apply to all possible inputs. Accuracy is therefore evaluated only on a subset of the testing data to which a given explanation apply. Proportion of the data to which the explanation apply is called coverage and is calculated as:

\begin{equation}\label{eq:cov}
\frac{\textbar\Lambda_{test}\textbar}{\textbar\Pi_{test}\textbar}\text{.}
\end{equation}

For Anchors, the subset $\Lambda_{test}$ contains the testing instances that satisfy explanation's rule conditions. For LIME, the subset $\Lambda_{test}$ contains the testing instances for which the explanation produces confident predictions. The threshold for confident predictions was set differently for each explained model. SHAP's accuracy and coverage can not be evaluated as the method does not produce a local surrogate model as an explanation.

\subsection{Results}

\autoref{tab:res_ad} shows results of the faithfulness evaluation. 2000 samples were generated for each artificial dataset, with half of the samples used for training and other half for testing. The same sets of training and testing data were used for the models being explained and for the explanations. Explanations were generated for each sample in the test set and the values listed in the table are mean metric values of all the explanations. For each artificial dataset, the chosen family of machine learning models is specified and accuracy of the model being explained is given in parentheses. Models' hyperparameters were set manually based on knowledge of the artifical dataset and explanations' parameters were set to default.

On the condInd dataset, all the explanation methods produced faithful local explanations. Anchors achieved a seemingly low recall on the important features as its goal is to maximize explanation's coverage while producing an accurate explanation. To achieve this goal on the Naive Bayes classifier trained on the condInd dataset, Anchors does not have to use all of the important features in its explanations. For example, when the features that have the same value as class in 90 and 70\% of the cases have equal value, the model predicts this value as the class regardless of the other features.

\newpage

\begin{table}[ht!]
\resizebox{\textwidth}{!}{
{ \renewcommand{\arraystretch}{1.5}
\begin{tabular}{c c | c | c | c | c }
\hline
Explanation & & condInd & xor & cross & groups \\
method& & NB (0.92) & DT (1.00) & RF (1.00) & KNN (0.94) \\
\hline
\multirow{5}{*}{LIME}& Imp Recall & 0.99 & 0.81 & 0.67 & 0.54\\
& Unimp FDR & 0.01 & 0.19 & 0.33 & 0.46 \\
& MAE & 0.053 & 0.105 & 0.213 & 0.240 \\
& Accuracy & 0.93 & $\emptyset$ & $\emptyset$ & $\emptyset$ \\
& Coverage & 0.55 & 0.0 & 0.0 & 0.0 \\
\hline
\multirow{5}{*}{Anchors}& Imp Recall & 0.62 & 1.0 & 1.0 & 0.99\\
& Unimp FDR & 0.06 & 0.71 & 0.39 & 0.82\\
& MAE & / & / & / & /  \\
& Accuracy & 0.92 & 1.0 & 0.97 & 0.88\\
& Coverage & 0.36 & 0.06 & 0.13 & 0.02\\
\hline
\multirow{5}{*}{SHAP}& Imp Recall & 0.79 & 1.0 & 1.0 & 0.95 \\
& Unimp FDR & 0.0 & 0.0 & 0.05 & 0.27\\
& MAE & 0.046 & 0.008 & 0.025 & 0.082\\
& Accuracy & / & / & / & / \\
& Coverage & / & / & / & / \\
\hline
\end{tabular}
}}
\caption{Results of faithfulness evaluation on artificial datasets}
\label{tab:res_ad}
\end{table}

On the xor dataset, LIME failed to produce faithful local surrogate models, despite being able to identify the important features and assigning reasonably accurate weights to the features. LIME explanations failed to produce any confident predictions which resulted in 0 coverage. This behaviour was expected as linear models are unable to learn even the two variable version of the xor function since it is highly non-linear \cite{Goodfellow-et-al-2016}. Anchors produced explanations with low coverage due to the inclusion of unimportant features. Anchors produces the explanation rules iteratively by adding one feature condition to the rule in each step. However in the xor dataset, all feature conditions have the same discriminative value unless two of the important features are already included in the Decision Rule, which causes the high false discovery rate of unimportant features. While the xor dataset proved to be difficult for both LIME and Anchors methods, SHAP explanations achieved nearly optimal faithfulness. 

\newpage

The cross dataset is similar to the xor dataset with only two important features, where the features are continuous instead of binary. This causes similar issues for both LIME and Anchors methods. Looking at the \autoref{fig:cross}, the optimal coverage of the Anchors explanations should be 0.25 as opposed to the achieved coverage of 0.13. 

Similar issues for LIME and Anchors also emerged on the groups dataset, where even the SHAP method included some of the unimportant features in its explanations, unlike on the other artificial datasets. 

\subsection{Summary}
In this section, local model-agnostic explanation methods -- LIME, Anchors and SHAP were evaluated in terms of their faithfulness to the model being explained on four artificial datasets. The artificial datasets demonstrated situations where both LIME and Anchors produce suboptimal explanations, whereas SHAP was able to produce faithful explanations on all the datasets.  

LIME fails to produce faithful local explanations of highly non-linear models. Anchors produces overly specific explanations with low coverage when individual features have low discriminative value. Out of the evaluated local model-agnostic explanation methods, SHAP was evaluated to be the most robust one.
