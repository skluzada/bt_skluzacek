\section{Explaining a black-box classifier learned on real-world dataset}
In this section, the local model-agnostic methods are used to explain a black- -box classifier learned on the real-world UCI divorce dataset \cite{nevsosbilen549416}. 

The divorce dataset contains 51 features, each corresponding to a certain statement about a participant and their partner (current or ex), such as ``I~enjoy traveling with my wife" or ``My spouse and I have similar ideas about how marriage should be". The feature values range from $-2$ to $2$, where $-2$ means the participant strongly disagrees with the statement and $2$ means the participant strongly agrees with the statement. The class is binary, where 0 corresponds to a divorced participant and 1 corresponds to a married participant. The dataset contains data on 170 participants (84 divorced and 86 married) and was split into 113 training instances and 57 testing instances. 

Y\"{o}ntem et. al. in \cite{nevsosbilen549416} have shown that Random Forest models achieve the highest performance on this task. The Random Forest model consisting of 50 Decision Trees with maximum depth of 4 achieved 98.2\% accuracy on the set of testing data. The hyperparameters were found using grid search and cross-validation. 

\newpage

Y\"{o}ntem et. al. have also compared performance of many different divorce prediction tools, concluding that the Random Forest models learned on this dataset achieves the highest accuracy, while also not requiring personal meeting with a couple therapist. However, a notable downside of this automated approach is that potential users are not expected to be able to understand decision making of a model that consists of 50 Decision Trees and uses 51 different features. It is easy to imagine that a couple would be more interested in the negative influences in their marriage that they can work on rather than getting an accurate prediction that they will divorce. 

LIME, Anchors and SHAP explanations of one selected instance are shown below. The instance being explained is correctly predicted as divorced by the Random Forest model (the predicted probability of class 0 is 92\%).

\autoref{tab:LIME_divorce} shows LIME explanation of an instance predicted as divorced by the model. The number of features used by the LIME explanation was set to 7. 

\begin{table}[ht!]
\centering
\resizebox{\textwidth}{!}{
{ \renewcommand{\arraystretch}{1.2}
\begin{tabular}{c | c | c | c}
\hline
\multirow{2}{*}{Feature Statement} & \multirow{2}{*}{Value} & \multirow{2}{*}{Weight} & Feature's\\
& & & contribution \\
\hline
My spouse and I have similar ideas & \multirow{2}{*}{-1} & \multirow{2}{*}{0.090} & \multirow{2}{*}{-0.090} \\
about how marriage should be. & & \\

My spouse and I have similar ideas & \multirow{2}{*}{-2} & \multirow{2}{*}{0.028} & \multirow{2}{*}{-0.057} \\
about how roles should be in marriage. & & \\

We share the same views about & \multirow{2}{*}{-2} & \multirow{2}{*}{0.014} & \multirow{2}{*}{-0.028} \\
being happy in our life with my spouse.  & & \\

I think that one day in the future, when I look back, & \multirow{2}{*}{-1} & \multirow{2}{*}{0.024} & \multirow{2}{*}{-0.024} \\
I see that my spouse and I have been in harmony with each other. & & \\

I know my spouse's basic anxieties. & \multirow{1}{*}{-2} & \multirow{1}{*}{0.008} & \multirow{1}{*}{-0.015} \\

I enjoy traveling with my wife. & \multirow{1}{*}{-2} & \multirow{1}{*}{0.007} & \multirow{1}{*}{-0.015} \\

My spouse and I have similar values in trust. & \multirow{1}{*}{-2} & \multirow{1}{*}{0.005} & \multirow{1}{*}{-0.011} \\
\hline
Explanation model's intercept & \multicolumn{3}{c}{0.436} \\
\multirow{1}{*}{Explanation model's prediction} & \multicolumn{3}{c}{$0.191 \Rightarrow 0$} \\ 
\end{tabular}
}}
\caption[LIME explanation of the instance predicted as divorced]{LIME explanation of the instance predicted as divorced. Linear model's prediction is a sum of intercept and features' contributions, where feature's contribution is a multiplication of feature's value and corresponding weight. The LIME surrogate model's prediction is lower than 0.5 therefore it predicts class 0 (divorced).}
\label{tab:LIME_divorce}
\end{table}

\autoref{tab:Anchors_divorce} shows Anchors explanation of the same instance predicted as divorced by the model. Anchors is able to produce a very sparse explanation of the model's prediction, using only 2 of the 51 features. Method's parameters were set to default.

\newpage

\begin{table}[ht!]
\centering
\resizebox{\textwidth}{!}{
{ \renewcommand{\arraystretch}{1.2}
\begin{tabular}{c c }
\hline
\multicolumn{2}{c}{Feature conditions} \\
\hline
Our dreams with my spouse are similar and harmonious. & <= 1 \\
We share the same views about being happy in our life with my spouse. & <= -2 \\
\hline
Explanation model's prediction & 0 \\
Explanation model's coverage & 0.47 \\
\hline
\end{tabular}
}}
\caption[Anchors explanation of the instance predicted as divorced]{Anchors explanation of the instance predicted as divorced. When all the feature conditions are satsified by an instance, the explanation predicts class 0 (divorced). The explanation rule is satisfied by 47\% instances from the test data.}
\label{tab:Anchors_divorce}
\end{table}

\autoref{tab:SHAP_divorce} shows the 7 highest Shapley values genereated by the SHAP method for the same instance predicted as divorced. The SHAP method assigned non- -zero Shapley values to 19 features. However, when exhaustive explanations are not required, shorter explanations should be preferred as they are easier to understand. 

\begin{table}[ht!]
\centering
\resizebox{\textwidth}{!}{
{ \renewcommand{\arraystretch}{1.2}
\begin{tabular}{c | c | c }
\hline
\multirow{2}{*}{Feature Statement} & \multirow{2}{*}{Value} & Feature\\
& & importance \\
\hline
My spouse and I have similar ideas & \multirow{2}{*}{-1} & \multirow{2}{*}{0.08} \\
about how marriage should be. & & \\

My spouse and I have similar ideas & \multirow{2}{*}{-2} & \multirow{2}{*}{0.07} \\
about how roles should be in marriage. & & \\

We're compatible with my spouse about  & \multirow{2}{*}{-1} & \multirow{2}{*}{0.04} \\
what love should be.  & & \\

We share the same views about & \multirow{2}{*}{-2} & \multirow{2}{*}{0.04} \\
being happy in our life with my spouse. & & \\

I think that one day in the future, when I look back, & \multirow{2}{*}{-1} & \multirow{2}{*}{0.03} \\
I see that my spouse and I have been in harmony with each other. & & \\

I know my spouse's basic anxieties. & \multirow{1}{*}{-2} & \multirow{1}{*}{0.03} \\

My spouse and I have similar values in trust. & \multirow{1}{*}{-2} & \multirow{1}{*}{0.03} \\

If one of us apologizes when our discussion & \multirow{2}{*}{-1} & \multirow{2}{*}{0.02} \\

deteriorates, the discussion ends. & & \\
\hline
Explained model's expected prediction (probability of class 0) & \multicolumn{2}{c}{0.47} \\
\multirow{1}{*}{Explained model's prediction (probability of class 0)} & \multicolumn{2}{c}{$0.92$} \\ 
\end{tabular}
}}
\caption[7 highest Shapley values of the instance predicted as divorced generated by the SHAP method.]{7 highest Shapley values of the instance predicted as divorced generated by the SHAP method. The model's expected probability of predicting class 0 is 0.47, whereas for this instance the predicted probability of class 0 is 0.92. Shapley values explain the difference between the expected prediction and the prediction for a particular instance.} 
\label{tab:SHAP_divorce}
\end{table}

\newpage

\subsection{Faithfulness evaluation}

To evaluate fathfulness of explanations of a black-box model trained on a real-world dataset, metrics such as Recall on important features or MAE from \hyperref[ad_metrics]{subsection 2.2.2} can not be used, as the correct explanations are usually unknown. LIME and Anchors produce local surrogate models as explanations, which can be evaluated in terms of accuracy and coverage even on a real- -world datasets. The only metric that could be used to evaluate faithfulness of SHAP explanations is described in \hyperref[sec:faith_eval_spec]{subsection 1.5.1.2} and based on replacing feature values with no information values (called no-op values) and measuring the impact on the predictions of the model being explained. The only sensible no-op value in the divorce dataset is 0 as it represents a neutral answer. However, for the model being explained, features with 0 value have positive impact towards not divorced predictions. When a considered instance is predicted as not divorced, replacing its feature values with no-op value changes the model's predictions for only 3 features, while the SHAP method assigns non-zero Shapley values to 19 features. This metric was therefore deemed as unreliable for evaluating SHAP explanations' faithfulness with respect to the model trained on the divorce dataset. 

\autoref{tab:res_rw} show results of accuracy and coverage evaluation of LIME and Anchors explanations. Explanations were generated for each sample in the test set and mean values of accuracy and coverage are listed in the table. Both explanation methods achieved high accuracy therefore their explanations are considered faithful to the model being explained. LIME explanations achieved higher coverage, thus they explain a bigger part of the model, while Anchors explanations achieved better comprehensibility by using only 2 feature conditions in each explanation.

\vspace{0.7cm}

\begin{table}[ht!]
\centering
{ \renewcommand{\arraystretch}{1.2}
\begin{tabular}{c c | c }
\hline
\multicolumn{1}{c}{Explanation method} & \multicolumn{2}{c}{\quad}\\
\hline
\multirow{2}{*}{LIME} & Accuracy & 1.0 \\
& Coverage & 0.63 \\
\hline
\multirow{2}{*}{Anchors} & Accuracy & 0.98 \\
& Coverage & 0.34 \\
\hline
\end{tabular}
}
\caption{Results of faithfulness evaluation of LIME and Anchors explanations on real-world divorce dataset}
\label{tab:res_rw}
\end{table}

\newpage
