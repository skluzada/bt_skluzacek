\section{Global model-agnostic explanation methods}\label{sec:global}
Global model-agnostic explanation methods aim to explain a machine learning model for all possible inputs by studying the model only through its inputs and corresponding outputs. While it is usually unatainable to globally explain a complex model, the global surrogate explanation model is described in the section below as many local explanation methods are build upon the same idea.

\subsection{Global surrogate explanation model}

A global surrogate model is a machine learning model that approximates a Black-box machine learning model that is being explained. The idea is that when the global surrogate model is a White-box model and accurately approximates the model that is being explained, then it can be used as an explanation of the underlying Black-box model. 

The pseudocode of generating a global surrogate explanation model is given in \hyperref[alg:glob]{Algorithm 1}. To generate an explanatory global surrogate model an appropriate family of machine learning model, training algorithm and its hyperparameters have to be chosen first, in order to provide a model that is interpretable to a given audience. Next, a training set of data for training the surrogate model has to be chosen. The training set for the surrogate model can be arbitrarily large, since it is annotated by the underlying Black-box model. Often enough in order to prevent overfitting and achieve high generalization, a model requires higher capacity than is necessary for the task due to the limited number of the training examples. Thanks to the theoretically unlimited number of training examples for the surrogate model, it is possible for the surrogate model to approximate the Black-box model while remaining interpretable~\cite{Goodfellow-et-al-2016}. Note that this is possible only in cases when the function represented by the Black-box model is not too complex to be represented by a White-box model as illustrated in \autoref{fig:complex_function}.

It is neccessary to choose a testing set for the surrogate model, also annotated by the model being explained, and an accuracy metric to use to asses the quality of surrogate model's approximation. Finally, an accuracy threshold bellow which the surrogate model is insufficient as an explanation should be chosen. The optimal accuracy threshold value is unclear and even when the suroggate model's accuracy seems high, there can be a subset of data for which the surrogate model is highly inaccurate~\cite{molnar2019}.


\begin{algorithm}[H]
\caption{Global surrogate explanation model}\label{alg:glob}
\begin{algorithmic}[1]
\Require Machine learning algorithm $A$ with hyperparameters $h$
\Require Training set $\varPi_{train}$  and testing set $\varPi_{test}$ annoted by the model being explained
\Require Accuracy metric $accuracy$ and accuracy threshold $t$
\State $g \leftarrow A_h(\varPi_{train})$ \Comment{Train the global surrogate model}
\If{$accuracy(g(\varPi_{test})) > t$} \Comment{Accuracy condition}

    \Return $g$
\Else

    \Return "Insufficient accuracy, choose a different machine learning algorithm or generate additional training data"
\EndIf
\end{algorithmic}
\end{algorithm}
