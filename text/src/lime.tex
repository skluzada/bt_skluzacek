\subsection{LIME}\label{sec:LIME}
LIME is a local model-agnostic explanation method created by Ribeiro et. al.~\cite{ribeiro2016should}. LIME explains a given prediction of a Black-box classifier by producing a sparse linear model that is locally faithful the Black-box model. LIME is essentially a linear local surrogate model that tries to approximate the Black-box model around the input of a given prediction, rather than trying to approximate the Black-box model for all the possible inputs like global surrogate models. 

\hyperref[sec:lime_fw]{Subsection 1.8.1.1} describes a general framework for producing a local surrogate explanation models. This framework was originally referred to as LIME with Sparse linear explanations beeing only an instance of the framework that uses linear model as the surrogate model. However, after the paper was published, the Sparse linear explanations began to be called LIME even by the authors themselves~\cite{ribeiro2018anchors}.

\newpage

\subsubsection{Local surrogate explanation model}\label{sec:lime_fw}
The goal of a local surrogate model is to approximate a Black-box model only around a given prediction, therefore the locality around the prediction has to be emphasized during the training of the surrogate model. This can be achieved either by sampling the training data only around the corresponding input of the prediction or by weighting the training samples by some proximity measure. LIME uses the later approach where the training data for the surrogate model are sampled from the distribution of the training data of the model being explained and weighted by a proximity measure such that instances farther from the input being explained are assigned lower weights. Each of the input features are sampled individually from Gaussian distribution with their means and standard deviations taken from the training data of the underlying Black-box model. 

Formally, let $f:\mathbb{R}^n\mapsto{}\mathbb{R}$ denote the model being explained, $x\in\mathbb{R}^n$ denote the input of the prediction being explained, $f(x)$ denote the corresponding prediction (class label or probability of a class) and $\pi_x:\mathbb{R}^n\mapsto{}\mathbb{R}$ denote the proximity measure between $x$ and another instance from $\mathbb{R}^n$. 

Furthemore, let $g\in{}G$ denote an explanatory local surrogate model that belongs to a family of potentialy White-box models~$G$ such as Decision Rule or Linear Regression. As discussed in \autoref{sec:box} every family of machine learning models contains both White-box and Black-box models. Therefore in order to ensure that the surrogate model is interpretable, let~$\Omega(g)$ denote a complexity metric of a model~$g$. The choice of a complexity metric depends on the chosen family of models~$G$, for Decision Rules $\Omega(g)$ may be the number of conditions, while for Linear Regression, $\Omega(g)$ may be the number of features with non-zero weights.

Finally, let $\mathcal{L}(f,g,\pi_x)$ be a loss function that measures how unfaithful the surrogate model~$g$ is in approximating the underlying Black-box model~$f$ in the locality defined by the proximity measure~$\pi_x$. The explanatory local surrogate model is then given by:
\begin{equation}\label{eq:loc_surr}
\hat{g}(x) = \argmin_{g\in{}G} \mathcal{L}(f,g,\pi_x) + \Omega(g)
\end{equation}
This formulation optimizes both interpretability and local faithfulness in order to find an optimal trade-off between interpretability and accuracy as discussed in \autoref{sec:tradeoff}. Thresholds can also be introduced to guarantee a certain level of interpretability or local faithfulness. 

\subsubsection{Sparse linear explanations}\label{subsec:lime_fw}
The \autoref{eq:loc_surr} allows for arbitrary choice of a family of machine learning models $G$, local faithfulness measure $\mathcal{L}$, proximity measure $\pi_x$ and complexity metric $\Omega$. This section describes the configuration chosen by Ribeiro et. al. originally called Sparse linear explanations that is implemented in the LIME framework available on \href{http://github.com/marcotcr/lime}{GitHub}. The illustrative example of Sparse linear explanation (LIME) is given in \autoref{fig:LIME}.

\begin{figure}[ht!]
\centering
\begin{tikzpicture}
\begin{axis}[
    width=\textwidth,
    height=8cm,
    axis x line*=bottom,
    axis y line*=left,
    axis lines=left,
    xmin=0,
    xmax=1,
    ymin=0,
    ymax=1,
    xtick=\empty,
    ytick=\empty,
    xticklabel=\empty,
    yticklabel=\empty,
]
    \addplot[
        scatter,
        only marks,
        point meta=explicit symbolic,
        scatter/classes={
            x={mark=oplus, green, mark size=8pt},
            p_6={mark=+, green, mark size=8pt},
            p_5={mark=+, green, mark size=5pt},
            p_4={mark=+, green, mark size=4pt},
            p_2={mark=+, green, mark size=2pt},
            m_6={mark=-, red, mark size=8pt},
            m_5={mark=-, red, mark size=5pt},
            m_4={mark=-, red, mark size=4pt},
            m_2={mark=-, red, mark size=2pt}
        },
    ] table [meta=label] {
    x       y       label
    0.45    0.50    x
    0.42    0.44    p_6
    0.50    0.53    p_6
    0.39    0.56    m_6
    0.35    0.25    p_5
    0.30    0.50    m_5
    0.42    0.70    m_5
    0.54    0.72    p_5
    0.20    0.57    m_4
    0.20    0.30    m_4
    0.53    0.11    p_4
    0.72    0.72    m_4
    0.22    0.93    p_4
    0.10    0.50    m_4
    0.50    0.90    m_4
    0.15    0.65    p_4
    0.80    0.60    m_4
    0.66    0.45    m_4
    0.65    0.07    p_4
    0.58    0.33    p_4
    0.86    0.66    m_2
    0.94    0.80    m_2
    0.90    0.05    p_2
    0.85    0.22    p_2
    0.93    0.45    p_2
    0.09    0.13    m_2
    0.90    0.90    p_2
    0.05    0.79    p_2 
    0.91    0.65    m_2
    0.05    0.34    m_2
    0.95    0.20    p_2
    };
    \addplot[mark=none, draw=green!9, fill=green!9] plot coordinates{
    (0.002, 0.005)
    (0.002, 0.98)
    (0.987, 0.98)
    (0.987, 0.005)
    (0.002, 0.005)
    };
    \addplot[mark=none, smooth, draw=red!12, fill=red!12] plot coordinates{
    (0.08, 0.81)
    (0.2, 0.62)
    (0.03, 0.55)
    (0.07, 0.12)
    (0.31, 0.07)
    (0.30, 0.24)
    (0.40, 0.50)
    (0.53, 0.80)
    (0.60, 0.65)
    (0.63, 0.14)
    (0.72, 0.16)
    (0.77, 0.50)
    (0.95, 0.58)
    (0.98, 0.93)
    (0.88, 0.82)
    (0.62, 0.93)
    (0.44, 0.95)
    (0.20, 0.80)
    (0.12, 0.95)
    (0.07, 0.89)
    (0.06, 0.83)
    };
    \addplot[mark=none, violet, style=dashed, thick] coordinates {(0.197, 0) (0.598, 0.99)};
    \node[violet!70, font=\large] at (axis cs:0.28,0.13) {{$g$}};
    \node[green!70, font=\large] at (axis cs:0.04,0.06) {{$f$}};
\end{axis}
\end{tikzpicture}
\caption[Illustration of Sparse linear explanation (LIME)]{Illustration of Sparse linear explanation (LIME). The green and red background shows the decision boundary of the underlying Black-box classifier~$f$, the points in the space represent the samples for training the local surrogate model $g$ with sizes of the samples indicating their weights. In this example LIME is able to produce an explanation model $g$ (violet line) that is locally accurate around the instance being explained (circled point). However in cases where the underlying Black-box model is highly non-linear, LIME may be unable to produce an explanation that is locally faithful.}
\label{fig:LIME}
\end{figure}

The pseudocode of producing a sparse linear explanation (LIME) is given in \hyperref[alg:lime]{Algorithm 2}. As the name suggests the choice of $G$ is a family of Linear Regression models, where the explanatory local surrogate model is given by $g(x') = w_g^Tx'$. The $x'\in\mathbb{R}^d$ denotes a sparse representation of input $x\in\mathbb{R}^n$ that contains the $d~\leq~n$ most important features of $x$. The sparse representation is often required in order to produce an interpretable explanation, as the original representation~$x$ might be high dimensional. When $n$ is low enough $d$ can be set as $d = n$. The number of features used in the explanation is forced by the complexity measure $\Omega$ given by $\Omega(g) = \infty{}\mathbbm{1}_{\lVert{}w_g\rVert{}_{0} > d}$, where ${\lVert{}w_g\rVert{}}_{0}$ denotes the $\ell_0$ ``norm" defined as the number of non-zero elements of a vector.  This particular choice of $\Omega$ makes solving \autoref{eq:loc_surr} directly intractable as the number of possible feature combinations grows exponentially with $d$. Therefore an approximation is produced instead where $d$ of the most important features are preselected. The $d$ most important features are by default chosen by training a locally weighted Linear Regression with L2 regularization (Ridge Regression) and then selecting the $d$ features with the highest weights.

After the $d$ features are selected the explanatory model is obtained by minimizing the locally weighted square loss with L2 regularization  $\mathcal{L}$, where the weights are given by the  the exponential kernel $\pi_x$ on Euclidean distance $D$. 

\begin{equation}\label{eq:lime_L}
\mathcal{L}(f,g,\pi_x) = \sum_{z, z' \in{}\mathcal{Z}} 
exp\left(\frac{-D(x, z)^2}{\ell^2}\right)
{\left( f(z) - g(z') \right)}^2 +
\lambda{}{\lVert{}w_g\rVert{}}^{2}_{2}, 
\end{equation}

%\begin{equation}\label{eq:lime_L}
%\mathcal{L}(f,g,\pi_x) = \sum_{z, z' \in{}\mathcal{Z}} 
%\underbrace{exp\left(\frac{-D(x, z)^2}{\sigma^2}\right)}_\text{proximity measure $\pi_x$}
%\underbrace{
%\underbrace{{\left( f(z) - g(z') \right)}^2}_\text{square loss} + 
%\underbrace{\lambda{}{\lVert{}w_g\rVert{}}^{2}_{2}}_\text{L2 regularization}}
%_\text{Ridge Regression},
%\end{equation}

where $\mathcal{Z}$ denotes the training set for the explanatory model sampled from the Gaussian distribution $\mathcal{N}(\mu, \sigma^2)$ with the mean $\mu$ and variance $\sigma^2$ of each feature taken from the underlying model's training set and $\ell$ denotes the kernel width which is by default defined as $\ell=0.75\:\sqrt[]{d}$.

\begin{algorithm}[H]
\caption{LIME (Sparse linear explanations)}\label{alg:lime}
\begin{algorithmic}[1]
\Require Classifier $f$, Input $x$ of the prediction being explained
\Require Number of training samples $M$, Length of explanation $d$
\Require Means $\mu$ and variances $\sigma^2$ of all features from the classifier's $f$ training data
\State $\mathcal{Z} \leftarrow{} \{\}$
\For{$i \in \{1, 2, \dots, M\}$}
    \State $z_i \leftarrow \mathcal{N}(\mu, \sigma^2)$
    \State $\mathcal{Z}\cup\langle z_i, f(z_i), \pi_x(z_i) \rangle $
\EndFor
\State $g_{feature\_selection} \leftarrow$ Ridge$(\mathcal{Z})$
\State Select the $d$ features with the highest weights in $g_{feature\_selection}$
\State $g\leftarrow$ Ridge$(\mathcal{Z})$ 
\Comment{with $z'_i$ as features and $f(z)$ as target} 

\State \textbf{return} $g$ 
\end{algorithmic}
\end{algorithm}
