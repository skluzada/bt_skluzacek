\subsection{SHAP} \label{sec:SHAP}

Shapley Additive Explanations (SHAP) is a local model-agnostic explanation method that explains a given prediction of a Black-box model through feature importance scores. Feature importance explanation shows how each of the input's features influenced the prediction. Such explanations are popular as there are many model-agnostic and model-specific methods for calculating global or local feature importance scores \cite{arrieta2020explainable}. SHAP is based on Shapley values that come from cooperative game theory. Cooperative game is a game where multiple players cooperate in order to acquire a payout and Shapley values show how to fairly divide the payout between the individual players based on their contributions to the payout. When used on a machine learning model, the Shapley values show the influence that each feature has on the prediction.

Formally, a cooperative (or coalitional) game is a tuple $\langle P, v \rangle$, where $P$ is a finite set of players and $v:2^P \mapsto \mathbb{R}$ is a characteristic function that maps every possible subset of players $S\subseteq P$ (coalition) to a number representing coalition's collective payout. The characteristic function must satisfy that $v(\emptyset)=0$, where $\emptyset$ denotes the empty set of players. Intuitively, the marginal contribution to the payout of a player $i$ in a coalition $S$ could be obtained as the difference between the payouts of coalitions with and without the player~$i$:~$v(S\cup\{i\}) - v(S)$. However, this equation calculates the marginal contribution only in the situation when the player $i$ joins the coalition as the last player. The marginal contribution of the player $i$ may therefore be too low as the player's skillset could already be present in the coalition $S$ before player's addition to the coalition. Similarly, if the player $i$ is added as the first player to the coalition, the player's marginal contribution may be too high, bringing down other players' contributions. Shapley value $\phi_i(v)$ is defined as the average marginal contribution of the player $i$ over all possible permutations of the coalition. Shapley value $\phi_i(v)$ is given by:

\begin{equation}\label{eq:shapley}
\begin{split}
\phi_i(v) = \sum_{S\subseteq P\textbackslash\{i\}} \frac{\textbar S \textbar! (\textbar P \textbar - \textbar S \textbar - 1)!}{\textbar P \textbar!} (v(S\cup\{i\}) - v(S))\text{,}
\end{split}
\end{equation}

where $\textbar S \textbar$ denotes the number of elements of $S$. Subset $S$ represents players that were picked before the player $i$ and subset $P\textbackslash\{S\cup \{i\}\}$ represents players that were picked after the player $i$. Since the player $i$ is not affected by the exact order within the subsets of the players picked before and after the player $i$, the payout differences $v(S\cup\{i\}) - v(S)$ are weighted by the term $\nicefrac{\textbar S \textbar! (\textbar P \textbar - \textbar S \textbar - 1)!}{\textbar P \textbar!}$ that calculates how many permutations are represented by the particular pick of~$S$.

Lloyd Shapley has proved in \cite{shapley1953value} that the Shapley values are the only solution to the problem of fair distribution of the payout that satisfy the following axioms.

\textbf{Efficiency axiom}\quad The sum of all Shapley values is equal to the collective payout: 
\begin{equation}\label{eq:shap_ax1}
\sum_{i\in P} \phi_i(v) = v(P)
\end{equation}

\textbf{Symmetry axiom}\quad If two players increase the payout of every coalition by the same amount, then their Shapley values are equal:
\begin{equation}\label{eq:shap_ax2}
\forall i, j \in P: \forall S\subset P \:\: v(S\cup\{i\}) = v(S\cup\{j\}) \wedge i, j \notin S \Rightarrow  \phi_i(v) = \phi_j(v)
\end{equation}

\textbf{Dummy axiom}\quad If a player does not increase the payout of any coalition, then its Shapley value is equal to zero:
\begin{equation}\label{eq:shap_ax3}
\forall i \in P: \forall S\subset P \:\: v(S\cup\{i\}) = v(S) \Rightarrow  \phi_i(v) = 0
\end{equation}

\textbf{Additivity axiom}\quad If a characteristic function of a game can be represented as a sum of two charactaristic functions, then the corresponding Shapley values can be decomposed in the same way:  
\begin{equation}\label{eq:shap_ax4}
\begin{split}
\forall \langle P, v + w \rangle : \forall S\subset P \:\: (v+w)(S)=v(s) + w(S) \Rightarrow \\
\Rightarrow \forall i \in P \:\: \phi_i(v+w) = \phi_i(v) + \phi_i(w)
\end{split}
\end{equation}


These axioms demonstrate that SHAP explanations are built on a solid theory which may make them more appealing in some use cases or to some audiences~\cite{molnar2019}. Patrick Hall, who is a co-founder of an AI-focused law firm called bnh.ai, suggests using SHAP explanations in fields regulated by the law such as banks or insurance companies \cite{hall2018build}.

The \autoref{eq:shapley} can not be directly used to generate an explanation of a machine learning model as most of the machine learning models can not handle missing data and must be provided with a value for each input feature. Moreover, the \autoref{eq:shapley} requires evaluation of every subset which is unfeasible as the number of subsets grows exponentially with the number of players (features). 

Štrumbelj et. al. in \cite{vstrumbelj2014explaining, kononenko2010efficient} propose an approach to overcome both of these issues. The pseudocode of their algorithm for generating explanations based on Shapley values is given in \hyperref[alg:shap]{Algorithm 6}. First, an equivalent formulation of the \autoref{eq:shapley} that computes the average marginal contribution over the set of all ordered permutations $\Psi(P)$ of players $P$ is given by:

\begin{equation}\label{eq:shapley2}
\phi_i(v) = \frac{1}{\textbar P \textbar!} \sum_{\psi \in \Psi(P)}  (v\:\!(Pre^i(\psi)\cup\{i\}) - v\:\!(Pre^i(\psi)))\text{,}
\end{equation}

where $Pre^i(\psi)$ denotes the set of all players that precede the player $i$ in permutation $\psi \in \Psi(P)$.  

Let $f:\mathbb{R}^n\mapsto{}\mathbb{R}$ denote the model being explained, $x\in\mathbb{R}^n$ denote the input of the prediction being explained and $f(x)$ denote the corresponding prediction (class label or probability of a class). Let the set of players be represented as the set of $n$ features denoted by their corresponding feature indexes $N~=~\{1, 2, \dots n\}$ and the payout be represented as the difference between the prediction being explained $f(x)$ and the expected prediction of the model being explained $\mathbb{E}[f]$. The goal is to fairly divide the payout $f(x) - \mathbb{E}[f]$ between the individual features of input $x$ based on features' contributions. To simulate the situation where a subset of features is missing from the input, the values of the features that are supposed to be missing are replaced by randomly sampled values. Formally, for a permutation $\psi \in \Psi(N)$ and a sample $z\in\mathbb{R}^n$ drawn from a distribution $\mathcal{D}$ (distribution from which the training samples for the explanation are drawn) two instances $x_{+i}, x_{-i} \in \mathbb{R}^n$ are constructed as:

\begin{equation}\label{eq:instances}
\begin{split}
x_{+i} = (x_{\psi^{-1}(1)}, \:\! x_{\psi^{-1}(2)}, \:\! \dots, \:\! x_{\psi^{-1}(i)}, \:\! z_{\psi^{-1}(i+1)}, \:\! \dots, \:\!  z_{\psi^{-1}(n)}) \text{,}
\\
x_{-i} = (x_{\psi^{-1}(1)}, \:\! x_{\psi^{-1}(2)}, \:\! \dots, \:\! z_{\psi^{-1}(i)}, \:\! z_{\psi^{-1}(i+1)}, \:\! \dots, \:\!  z_{\psi^{-1}(n)}) \text{,}
\end{split}
\end{equation}

\newpage

Let~$\phi_i(f, x)$ denote the Shapley value of feature $i$ for the model $f$ and input $x$. To compute the exact value of $\phi_i(f, x)$, the whole set of ordered permutations~$\Psi(N)$ would have to be evaluated. Instead, only a fixed number of $M$ randomly sampled permutations $\psi \in \Psi(N)$ and samples $z\in\mathcal{D}$ are evaluated to produce an approximation $\widehat{\phi_i}(f, x)$:

\begin{equation}\label{eq:shap_approx}
\widehat{\phi_i}(f, x) = \frac{1}{M} \sum_{\substack{m=1 \\ \psi_m \in\:\!\Psi(N) \\ z_m\in\:\!\mathcal{D}}}^{M}  (f\:\!({(x_{+i})}_m) - f\:\!({(x_{-i})}_m))\text{,}
\end{equation}

where ${(x_{+i})}_m$ and ${(x_{-i})}_m$ denote instances constructed as in \autoref{eq:instances} based on permutation $\psi_m$ and sample $z_m$.

$\widehat{\phi_i}(f, x)$ is an unbiased and consistent estimator of $\phi_i(f, x)$ and is approximately normally distributed: $\widehat{\phi_i}(f, x) \approx \mathcal{N}(\phi_i(f, x), \frac{\sigma_i^2}{M_i})$, where $\sigma_i^2$ denotes the variance of the feature $i$ in the distribution $\mathcal{D}$ and $M_i$ denotes the number of samples used for calculating $\widehat{\phi_i}(f, x)$. As $\widehat{\phi_i}(f, x) - \phi_i(f, x) \approx \mathcal{N}(0, \frac{\sigma_i^2}{M_i})$, the number of samples needed to accurately approximate $\phi_i(f, x)$ depends solely on the feature's $i$ variance $\sigma_i^2$. Choosing the same number of samples for all the features is undesirable as the features are likely to have different variances. Therefore a minimal number of samples for each feature $M_{min}$ and a maximal number of samples for all the features $M_{max}$ are chosen. After each feature is sampled $M_{min}$ times, the algorithm chooses which feature to sample next by choosing the feature $j$ with the highest $(\frac{\sigma_j^2}{m_j} - \frac{\sigma_j^2}{m_j + 1})$, where $m_j$ denotes the number of samples for the feature $j$ drawn so far. The algorithm stop after drawing $M_{max}$ samples. This sampling strategy minimizes the squared loss $\sum_{i=1}^n(\widehat{\phi_i}(f, x) - \phi_i(f, x))^2$. The same approach can be used even in situations where the variances $\sigma_i^2$ are unknown as they can be estimated from the samples $z \in \mathcal{D}$ drawn so far for example by Knuth's incremental algorithm~\cite{knuth97}.

\newpage

\begin{algorithm}[H]
\caption{SHAP} \label{alg:shap}
\begin{algorithmic}[1]
\Require Classifier $f$, Input $x$ of the prediction being explained
\Require Distribution $\mathcal{D}$ of the training data for explanation 
\Require Minimal number of samples for each feature $M_{min}$
\Require Maximal number of samples for all the features $M_{max}$
\For{$i=1$ to $n$} \Comment{Initialization}
    \State $m_i \leftarrow 0$ 
    \State $\phi_i \leftarrow 0$ \Comment{$\widehat{\phi_i}(f, x)$}
\EndFor
\While{$\sum_{i=1}^{n} m_i < M_{max}$}
    \If{$\exists i: m_i < M_{min}$}
        \State{pick feauture $j$ to be sampled s.t. $m_j < M_{min}$ }
    \Else
        \State{pick feature $j$ to be sampled with the highest $(\frac{\sigma_j^2}{m_j} - \frac{\sigma_j^2}{m_j + 1})$}
    \EndIf
    \State{$\psi \leftarrow$ $\Psi(N)$}
    \State{$z \leftarrow \mathcal{D}$}
    \State{$x_{+i} \leftarrow (x_{\psi^{-1}(1)}, \:\! x_{\psi^{-1}(2)}, \:\! \dots, \:\! x_{\psi^{-1}(i)}, \:\! z_{\psi^{-1}(i+1)}, \:\! \dots, \:\!  z_{\psi^{-1}(n)})$}
    \State{$x_{-i} \leftarrow (x_{\psi^{-1}(1)}, \:\! x_{\psi^{-1}(2)}, \:\! \dots, \:\! z_{\psi^{-1}(i)}, \:\! z_{\psi^{-1}(i+1)}, \:\! \dots, \:\!  z_{\psi^{-1}(n)})$}
    \State{$\phi_i \leftarrow \phi_i + (f\:\!(x_{+i}) - f\:\!(x_{-i}))$}
    \State{$m_j \leftarrow m_j + 1$}
\EndWhile
\For{$i=1$ to $n$} 
    \State $\phi_i \leftarrow \frac{\phi_i}{m_i}$
\EndFor
\State \textbf{return} $\phi$ \Comment{Vector of Shapley values}
\end{algorithmic}
\end{algorithm}

\subsubsection{Kernel SHAP}
Kernel SHAP is a method for approximating Shapley values introduced by Lundberg et. al. in \cite{lundberg2017unified}. The authors argue that Kernel SHAP achieves similar accuracy while being computationally more efficient compared to the \hyperref[alg:shap]{Algorithm 6}. The goal of Kernel SHAP is to learn an explanation model~$g$:

\begin{equation}\label{eq:shap_approx}
g(z') = \phi_0 + \sum_{i=1}^{n} \phi_i z'_i \text{,}
\end{equation}

where $z' \in \{0,1\}^n$ denotes a coalition vector for an input of $n$ features where the value of ${z'}_i$ specify whether the feature $i$ is present ($z'_i = 1$) or absent ($z'_i = 0$) \cite{molnar2019}. The Shapley value $\phi_0$ denotes the expected prediction of the model being explained $\mathbb{E}[f]$. For the input $x$ of the prediction being explained, the coalition vector is defined as a vector of all ones: $x' = (1, 1, \dots, 1)$. The explanation model $g$ must satisfy the following:

\begin{equation}\label{eq:shap_approx}
f(x) = g(x') = \phi_0 + \sum_{i=1}^{n} \phi_i x'_i
\end{equation}

This equation resembles the linear explanation model and Shapley values are the only solution to this equation that satisfies the axioms in \hyperref[eq:shap_ax1]{Equations 1.11-1.14} as proved by Lloyd Shapley in \cite{shapley1953value}. Lundberg et. al. have proved, that the LIME framework described in \hyperref[sec:lime_fw]{subsection 1.7.1.1} is able to recover the Shapley values with the following choices of loss function $\mathcal{L}$, weighting kernel $\pi_{x'}$ and complexity metric $\Omega$: 

\begin{subequations}
\begin{align}
\Omega(g)&=\:\!0\text{,} \\
\pi_{x'}(z')&=\frac{(n-1)}{(n\:choose\:{\lVert{}z'\rVert{}}_{0}){\lVert{}z'\rVert{}}_{0}(n-{\lVert{}z'\rVert{}}_{0})} \text{,} \\
\mathcal{L}(f, g, \pi_{x'}) &= \sum_{z' \in \mathcal{B}} \pi_{x'}(z') (f(h_x(z')) - g(z'))^2\text{,}
\end{align}
\label{eq:kershap}
\end{subequations}

where where ${\lVert{}z'\rVert{}}_{0}$ denotes the $\ell_0$ ``norm" defined as the number of non-zero elements of vector $z'$, $\mathcal{B}$ denotes a distribution of binary vectors and $h_x:~\{0,1\}^n~\mapsto~\mathbb{R}^n$ is a function that maps a coalition vector to the original feature space in the following way:

\begin{equation}\label{eq:shap_approx}
{(h_x(z'))}_i =
\begin{cases}
    x_i \in x &\text{if} \: z'_i = 1 \\
    z_i \in z &\text{if} \: z'_i = 0 \\ 
\end{cases}
\text{,}
\end{equation}

where ${(h_x(z'))}_i$ denotes the value of $h_x(z')$ on index $i$ and $z$ denotes a sample drawn from a distribution $\mathcal{D}$. The function $h_x$ therefore replaces the feature values of $x$ that are supposed to be absent with a value from a random sample $z$ which is a similar approach as in the \hyperref[alg:shap]{Algorithm 6}.

In the LIME framework for linear models the complexity metric $\Omega$ is supposed to limit the number of features used by the explanation model to ensure interpretability. For calculating Shapley values the complexity metric $\Omega$ is ommited as the Shapley values must be calculated for all the input features to ensure that the axioms in \hyperref[eq:shap_ax1]{Equations 1.11-1.14} are satisfied. The intuition behind the weighting kernel $\pi_{x'}$ is similar as in the weighting term in \autoref{eq:shapley}, where the weights are assigned based on how many permutations are represended by the particular pick of present and absent players (features). $\pi_{x'}$ is therefore not a proximity measure that defines the locality around the input $x$ as in the LIME framework. Finally, the linear explanation model whose weights correspond to Shapley values is obtained by minimizing the weighted square loss $\mathcal{L}$. The pseudocode of Kernel SHAP is given in \hyperref[alg:kernelshap]{Algorithm 7}.

\begin{algorithm}[H]
\caption{Kernel SHAP} \label{alg:kernelshap}
\begin{algorithmic}[1]
\Require Classifier $f$, Input $x$ of the prediction being explained
\Require Distribution $\mathcal{D}$ of the training data for explanation 
\Require Distribution $\mathcal{B}$ of binary vectors
\Require Number of training samples $M$
\State $\mathcal{Z}\leftarrow \emptyset$ \Comment{Training data}

\For{$i \in \{1, 2, \dots, M\}$}
    \State $z' \leftarrow \mathcal{B}$
    \State $z \leftarrow \mathcal{D}$
    \State $\mathcal{Z}\cup\langle z', f(h_x(z')), \pi_{x'}(z') \rangle $
\EndFor
\State{$\phi \leftarrow$ LinearRegression($\mathcal{Z}$)}
\State \textbf{return} $\phi$
\end{algorithmic}
\end{algorithm}

It is important to note that the linear explanation model returned by Kernel SHAP is not a local surrogate explanation model as it is designed to approximate Shapley values and not to approximate the model being explained. 

While Kernel SHAP was designed to be computationally more efficient than the \hyperref[alg:shap]{Algorithm 6}, Molnar et. al. in \cite{molnar2019} argue, that Kernel SHAP is still very time consuming for some practical applications. Lundberg et. al. in \cite{lundberg2017unified} propose and review several model-specific methods for approximating Shapley values such as Linear SHAP for linear models, Tree SHAP for tree-based models and Deep SHAP for Artificial Neural Networks. These methods achieve higher computational efficiency as they are allowed to access model's internals such as gradient values. By being based on the same solid theory of Shapley values, these model-specific explanation methods can also be used as a complementary method for selection of the better performing model.   
