\section{Explanation methods taxonomy} \label{sec:taxonomy}
Explanation methods are primarily divided based on the scope of their explanations -- whether they explain a machine learning model as a whole or only partially, and by model specificity -- whether they can be applied to any machine learning model or only to a particular family of machine learning models.

Explanation methods can be further divided based on the type of their explanations (feature importance scores, Decision Rules, etc.), whether they can be applied to any data or only to tabular/text/image data and whether they can be applied on classification and/or regression tasks.

\subsection{Explanation scope}

\subsubsection{Global explanation methods}
Global explanation methods aim to explain a model as a whole. In other words they aim to  help an audience to understand the full logic of the model~\cite{guidotti2018survey}. The audience that is provided a global explanation should also be able to correctly simulate the model’s prediction for any input. Global explanations can therefore provide new valuable insight and improve humans decision making in tasks where machine learning models generally outperform humans. However, it is often infeasible to provide an accurate and succinct global explanation of a complex model.

\newpage{}

\subsubsection{Local explanation methods}
Local explanation methods aim to explain a single prediction of a model. The idea of local explanation methods is that while a given model is globally too complex to be explained succinctly, explaining model's decision making for an individual prediction makes the explanation task feasible~\cite{ribeiro2016should}. Locally the predictions might only depend linearly or monotonically on some subset of features rather than having a complex dependence on all the features~\cite{molnar2019}.

Local explanation of a single prediction may provide understanding of the model’s global decision making, however, it is not sufficient to evaluate and assess trust in the model as  whole. Local explanations of multiple predictions can provide some insight into the global decision making of the model if the chosen set of explanations is non-redundant and representative. Choosing such set of explanations may be a feasible task even when providing a global explanation of the model is unfeasible~\cite{ribeiro2016should}.

Local explanation methods are also useful in tasks where only explanations of single predictions are required. For example in loan-approval systems, financial institutions are obligated by law in both European Union and United States to provide a reason why a given loan application was denied~\cite{eu2016gdpr, us1972cfr}. Explanation of a single loan application denial corresponds to a local explanation, while global explainability can be sacrificed for the sake of better performance.

\subsection{Model specificity}

\subsubsection{Model-agnostic explanation methods}
Model-agnostic explanation methods treats a model being explained as a~black- -box function. In this case term black-box does not refer to a complex uninterpretable model but rather to a model that can be studied only through its inputs and corresponding outputs without any knowledge of it’s internals~\cite{ribeiro2016model}. Model-agnostic explanation methods therefore provide a model flexibility as they can be applied to any predictive system regardless of its complexity or inner workings.

Model-agnostic methods also allow to study models that provide access only to its inputs and corresponding outputs via APIs. This limitation occurs in situations where a company treats a model as a proprietary software, thus is unwilling to release the whole model arguing that it contains company’s trade secrets. For example COMPAS (Correctional Offender Management Profiling for Alternative Sanctions) is a proprietary recidivism prediction tool that is in widespread use in the U.S. Justice system for predicting the probability that a criminal will be arrested again after their release. COMPAS was not created by any standard machine learning algorithm, it was rather designed by experts based on carefully designed surveys and expertise, however, its internals are kept as a trade secret. Even though it is not a machine learning model, it is still a predictive system that can be studied through model-agnostic methods~\cite{rudin2019stop}. Model-agnostic analysis of the COMPAS have shown that the system is racially biased against black people~\cite{propublica2016bias}.

When solving a particular machine learning task practitioners usually produce multiple models often generated by different machine learning algorithms. Competing models are subsequently compared in order to select the one that will have the best performance on the real data. This comparison is usually done by evaluating models' performances on a held-out subset of annotated data called test set. Relying solely on such evaluation can be misleading as model’s performance on the test set may not correspond to its performance once the model is deployed “in the wild”. One of many reasons for this might be an accidental information share between the sets training and testing data during preprocessing called Data leakage. Model-agnostic methods flexibility allows for two models generated by different machine learning algorithms to be explained by the same method. These explanations can then be used as a complementary method for comparison of the competing models~\cite{ribeiro2016model, ribeiro2016should}.

\subsubsection{Model-specific explanation methods}
Model-specific explanation methods are tied specifically to certain families of machine learning models and require full access to the model's internals. Their model specific design and ability to access model's internals (such as gradients) makes such methods generally more computationaly efficient than model-agnostic methods.

Model-specific explanation methods are especially interesting in the fields that are dominated by Deep learning, such as computer vision, natural language processing and one dimensional signal processing. The best performing models in such fields are almost exclusively versions of Artifical Neural Networks and therefore the flexibility of model-agnostic methods is unnecessary. Deep learning models benefits from the model-specific approach for two main reasons. First, Artifical Neural Networks learn features and concepts in their hidden layers that are not uncovered when using the model-agnostic approach. Second, the gradient can be utilized for higher computational efficiency~\cite{molnar2019}.

\newpage{}

